package com.paymentchain.transaction.controller;

import com.paymentchain.transaction.entities.Transaction;
import com.paymentchain.transaction.repository.ITransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/transaction")
public class TransactionController {


    private final ITransactionRepository iTransactionRepository;

    @Autowired
    public TransactionController(ITransactionRepository iTransactionRepository) {
        this.iTransactionRepository = iTransactionRepository;
    }

    @GetMapping()
    public List<Transaction> list() {
        return this.iTransactionRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Transaction> get(@PathVariable(name = "id") Long id) {
        Optional<Transaction> transaction = this.iTransactionRepository.findById(id);
        return transaction.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/customer/transactions")
    public List<Transaction> get(@RequestParam(name = "ibanAccount") String ibanAccount) {
        return iTransactionRepository.findByIbanAccount(ibanAccount);
    }

    @PostMapping
    public ResponseEntity<Transaction> post(@RequestBody Transaction input) {
        Transaction save = this.iTransactionRepository.save(input);
        return ResponseEntity.ok(save);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> put(@PathVariable Long id, @RequestBody Transaction input) {
        var message = "";
        Transaction newTransaction;
        var transactionById = this.iTransactionRepository.findById(id);
        if (transactionById.isPresent()) {
            newTransaction = transactionById.get();
            newTransaction.setAmount(input.getAmount());
            newTransaction.setFee(input.getFee());
            newTransaction.setDate(input.getDate());
            newTransaction.setChannel(input.getChannel());
            newTransaction.setDescription(input.getDescription());
            newTransaction.setReference(input.getReference());
            newTransaction.setStatus(input.getStatus());
            newTransaction.setIbanAccount(input.getIbanAccount());

            message = "Actualizado exitosamente, ".concat(newTransaction.toString());
        } else {
            newTransaction = input;
            message = "Se crea nueva transaction, ".concat(newTransaction.toString());
        }
        this.iTransactionRepository.save(newTransaction);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) {
        var transactionById = this.iTransactionRepository.findById(id);
        if (transactionById.isPresent()) {
            this.iTransactionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
