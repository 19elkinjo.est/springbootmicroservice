package com.paymentchain.customer.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.paymentchain.customer.entities.Customer;
import com.paymentchain.customer.entities.CustomerProduct;
import com.paymentchain.customer.repository.ICustomerRepository;
import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/v1/customer")
public class CustomerController {

    private final WebClient.Builder webClientBuilder;
    private final ICustomerRepository iCustomerRepository;

    @Autowired
    public CustomerController(WebClient.Builder webClientBuilder, ICustomerRepository iCustomerRepository) {
        this.webClientBuilder = webClientBuilder;
        this.iCustomerRepository = iCustomerRepository;
    }

    //web client config
    HttpClient client = HttpClient.create()
            //connection timeout: is a period within which a connection between a client and server must be established
            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
            .option(ChannelOption.SO_KEEPALIVE, true)
            .option(EpollChannelOption.TCP_KEEPIDLE, 300)
            .option(EpollChannelOption.TCP_KEEPINTVL, 60)
            //response timeout: the maximum time we wait to receive a response after sending a request
            .responseTimeout(Duration.ofSeconds(1))
            //read and write timeout: a read timeout occurs when no data was read within a certain
            //period of time, while the write timeout when a write operation cannot finish at specific time
            .doOnConnected(connection -> {
                connection.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS));
                connection.addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS));
            });

    @GetMapping()
    public List<Customer> list() {
        return this.iCustomerRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> get(@PathVariable Long id) {
        Optional<Customer> customer = this.iCustomerRepository.findById(id);
        return customer.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<Customer> post(@RequestBody Customer input) {
        input.getProducts().forEach(x -> x.setCustomer(input));
        Customer customer = this.iCustomerRepository.save(input);
        return ResponseEntity.ok(customer);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> put(@PathVariable Long id, @RequestBody Customer input) {
        var message = "";
        Customer newCustomer;
        var customerById = this.iCustomerRepository.findById(id);
        if (customerById.isPresent()) {
            newCustomer = customerById.get();
            newCustomer.setCode(input.getCode());
            newCustomer.setName(input.getName());
            newCustomer.setIban(input.getIban());
            newCustomer.setPhone(input.getPhone());
            newCustomer.setSurname(input.getSurname());
            message = "Actualizado exitosamente, ".concat(newCustomer.toString());
        } else {
            newCustomer = input;
            message = "Se crea nuevo customer, ".concat(newCustomer.toString());
        }
        this.iCustomerRepository.save(newCustomer);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) {
        var customerById = this.iCustomerRepository.findById(id);
        if (customerById.isPresent()) {
            this.iCustomerRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/full")
    public ResponseEntity<Customer> getByCode(@RequestParam String code) {
        var customer = this.iCustomerRepository.findByCode(code);
        if (customer != null) {
            List<CustomerProduct> products = customer.getProducts();
            //for each product find it name
            products.forEach(x -> {
                var productName = this.getProductName(x.getProductId());
                x.setProductName(productName);
            });
            //find all transactions that belong this account number
            List<?> transactions = this.getTransactions(customer.getIban());
            customer.setTransactions(transactions);
            return ResponseEntity.ok(customer);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Call Product Microservice , find a product by Id and return it name
     *
     * @param id of product to find
     * @return name of product if it was find
     */
    private String getProductName(long id) {
        var build = webClientBuilder.clientConnector(new ReactorClientHttpConnector(client))
                .baseUrl("http://localhost:8083/v1/product")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap("url", "http://localhost:8083/v1/product"))
                .build();

        var block = build.method(HttpMethod.GET).uri("/" + id)
                .retrieve().bodyToMono(JsonNode.class).block();

        if (block == null) {
            return "null, no se pudo capturar el name";
        }

        return block.get("name").asText();
    }

    /**
     * Call Transaction Microservice and Find all transaction that belong to the
     * account give
     *
     * @param iban account number of the customer
     * @return All transaction that belong this account
     */
    private List<?> getTransactions(String iban) {
        WebClient build = webClientBuilder.clientConnector(new ReactorClientHttpConnector(client))
                .baseUrl("http://localhost:8082/v1/transaction")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();

        Optional<List<?>> transactionsOptional = Optional.ofNullable(build.method(HttpMethod.GET)
                .uri(uriBuilder -> uriBuilder
                        .path("/customer/transactions")
                        .queryParam("ibanAccount", iban)
                        .build())
                .retrieve()
                .bodyToFlux(Object.class)
                .collectList()
                .block());

        return transactionsOptional.orElse(Collections.emptyList());
    }

}
