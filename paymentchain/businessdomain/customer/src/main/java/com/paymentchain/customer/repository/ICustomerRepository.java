package com.paymentchain.customer.repository;

import com.paymentchain.customer.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ICustomerRepository extends JpaRepository<Customer, Long> {

    @Query("SELECT c FROM Customer c WHERE c.code = ?1")
    Customer findByCode(String code);

    @Query("SELECT c FROM Customer c WHERE c.iban = ?1")
    Customer findByAccount(String iban);
}
