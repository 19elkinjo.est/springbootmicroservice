package com.paymentchain.product.controller;

import com.paymentchain.product.entities.Product;
import com.paymentchain.product.repository.IProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/product")
public class ProductController {

    private final IProductRepository iProductRepository;

    @Autowired
    public ProductController(IProductRepository iProductRepository) {
        this.iProductRepository = iProductRepository;
    }

    @GetMapping()
    public List<Product> list() {
        return this.iProductRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> get(@PathVariable Long id) {
        Optional<Product> product = this.iProductRepository.findById(id);
        return product.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<Product> post(@RequestBody Product input) {
        Product product = this.iProductRepository.save(input);
        return ResponseEntity.ok(product);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> put(@PathVariable Long id, @RequestBody Product input) {
        var message = "";
        Product newProduct;
        var byId = this.iProductRepository.findById(id);
        if (byId.isPresent()) {
            newProduct = byId.get();
            newProduct.setName(input.getName());
            newProduct.setCode(input.getCode());
            message = "Actualizado exitosamente, ".concat(newProduct.toString());
        } else {
            newProduct = input;
            message = "Se crea nuevo producto, ".concat(newProduct.toString());
        }
        this.iProductRepository.save(newProduct);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) {
        var customerById = this.iProductRepository.findById(id);
        if (customerById.isPresent()) {
            this.iProductRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
